function calc() {
    let a = document.getElementsByName("a");
    let b = document.getElementsByName("b");
    let result = document.getElementById("result");
    let re = /\D\./;
    if ((a[0].value.match(re) || b[0].value.match(re)) === null)
        result.innerHTML = ("Cтоимость вашего заказа: " + parseFloat(a[0].value, 10) * parseInt(b[0].value, 10));
    else result.innerHTML = "Ошибка! Неверный формат чисел";
    alert(result.innerHTML);
    return false;
}

window.addEventListener('DOMContentLoaded', function (event) {
    console.log("DOM fully loaded and parsed");
    let d = document.getElementById("my-button");
    d.addEventListener("click", calc);
});

function checkRadio() {
    let radioe = document.getElementsByName("color");
    radioe.forEach(function(radio) {
        radio.checked = false;
    });
}

function checkCheckbox(val) {
    let checkboxer = document.querySelectorAll("#checkboxes input");
    checkboxer.forEach(function(checkbox) {
        if (checkbox.value != val)
            checkbox.checked = false;
    });
}

window.addEventListener('DOMContentLoaded', function(event) {

    let cost = {
        form: [7999, 11299, 8999],
        color: [0, 800, 100],
        delivery: {
            delivery1: 1700,
            delivery2: 0,
        }
    };
    let form = document.getElementsByName("form");
    let radios = document.getElementById("radios");
    let checkbox = document.getElementById("checkboxes");
    let sum = document.getElementById("sum");
    let result = 0;
    var lastcostform = 0;
    radios.style.display = "none";
    checkbox.style.display = "none";
    form[0].addEventListener("change", function(event) {
        let select = event.target;
        result -= lastcostform;
        result += cost.form[select.value - 1];
        result -= lastcostcolor;
        lastcostcolor = 0;
        result -= lastcostborder;
        lastcostborder = 0;
        lastcostform = cost.form[select.value - 1];
        sum.innerHTML = ("Cумма к оплате: " + result + "₽");
        
        if (select.value == "1") {
            radios.style.display = "none";
            checkbox.style.display = "none";
            checkRadio();}
        else if (select.value == "2") {
            radios.style.display = "block";
            checkbox.style.display = "none";
            checkRadio();
        } else if (select.value == "3") {
            checkbox.style.display = "block";
            radios.style.display = "none";
            checkCheckbox(null);
        }
    });
    var lastcostcolor = 0;
    let radioe = document.getElementsByName("color");
    radioe.forEach(function(radio) {
        radio.addEventListener("change", function(event) {
            result -= lastcostborder;
            lastcostborder = 0;
            sum.innerHTML = ("Cумма к оплате: " + result + "₽");
            let r = event.target;
            if (radio.checked) {
                let optionPrice = cost.color[radio.value];
                if (optionPrice !== undefined) {
                    result -= lastcostcolor;
                    result += optionPrice;
                    lastcostcolor = optionPrice;
                }
                sum.innerHTML = ("Cумма к оплате: " + result + "₽");
            }
        });
    });
    var lastcostborder = 0;
    let checkboxer = document.querySelectorAll("#checkboxes input");
    checkboxer.forEach(function(checkbox) {
        checkbox.addEventListener("change", function(event) {
            result -= lastcostcolor;
            lastcostcolor = 0;
            sum.innerHTML = ("Cумма к оплате: " + result + "₽");
            let c = event.target;
            checkCheckbox(c.value);
            if (checkbox.checked) {
                let price = cost.delivery[c.value];
                if (price !== undefined) {
                    result -= lastcostborder;
                    result += price;
                    lastcostborder = price;
                }
                sum.innerHTML = ( "Cумма к оплате: " + result + "₽");
            }
        });
    });
});
